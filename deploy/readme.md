# README #

### How to deploy project for development? ###

* Download [VirtualBox][1] for your OS(Windows/Linux) and install.
* Download [Vagrant][2] for your OS(Windows/Linux) and install.

### Hown to install project? ###

* Cone it using git: _**git clone git@gitlab.com:go-do-it/server-api.git**_
* Open your terminal, go into "deploy" and run command: _**vagrant up**_
* Read instruction that will appear on your screen

### How do I connect to the new machine? ###

* Go to "deploy" directory and run command: _**vagrant ssh**_

### How to stop the machine and preserve the machine state? ###

* Go to "deploy" directory and run command: _**vagrant suspend**_

### How to start the machine again? ###

* Go to "deploy" directory and run command: _**vagrant up**_

### Provision production servers ###
* Go to "deploy/ansible" directory and run command: _**ansible-playbook playbook.yml -i hosts_production.yml**_

[1]: https://www.virtualbox.org/wiki/Downloads
[2]: https://www.vagrantup.com/downloads.html