<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     name="deliveries",
 *     indexes={
 *          @ORM\Index(columns={"identifier"}),
 *     }
 * )
 */
class Delivery
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned": true})
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="string", length=16777215)
     *
     * @Groups("result")
     */
    private $data;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return Delivery
     */
    public function setIdentifier($identifier): Delivery
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     * @return Delivery
     */
    public function setData($data): Delivery
    {
        $this->data = $data;

        return $this;
    }
}
