<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     name="products",
 *     indexes={
 *          @ORM\Index(columns={"identifier"}),
 *     }
 * )
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned": true})
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", nullable=false)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=false)
     *
     * @Groups("result")
     */
    private $url;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", nullable=false)
     *
     * @Groups("result")
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="price_expected", type="decimal", nullable=false)
     *
     * @Groups("result")
     */
    private $priceExpected;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return Product
     */
    public function setIdentifier($identifier): Product
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Product
     */
    public function setUrl($url): Product
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice($price): Product
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceExpected()
    {
        return $this->priceExpected;
    }

    /**
     * @param float $priceExpected
     * @return Product
     */
    public function setPriceExpected($priceExpected): Product
    {
        $this->priceExpected = $priceExpected;

        return $this;
    }
}