<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="steps")
 */
class Step
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned": true})
     * @ORM\GeneratedValue()
     *
     * @Groups("result")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="js_code", type="string", length=16777215)
     *
     * @Groups("result")
     */
    private $jsCode;

    /**
     * @var Action
     *
     * @ORM\ManyToOne(targetEntity="Action", inversedBy="steps")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private $action;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getJsCode(): string
    {
        return $this->jsCode;
    }

    /**
     * @param string $jsCode
     * @return Step
     */
    public function setJsCode($jsCode): Step
    {
        $this->jsCode = $jsCode;

        return $this;
    }

    /**
     * @return Action
     */
    public function getAction(): Action
    {
        return $this->action;
    }

    /**
     * @param Action $action
     * @return Step
     */
    public function setAction(Action $action): Step
    {
        $this->action = $action;

        return $this;
    }
}
