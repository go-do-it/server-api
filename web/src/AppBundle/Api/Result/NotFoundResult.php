<?php

namespace AppBundle\Api\Result;

use Symfony\Component\HttpFoundation\Response;

class NotFoundResult extends AbstractResult
{
    /**
     * @inheritDoc
     */
    public function __construct(string $message = null)
    {
        if (!$message) {
            $message = Response::$statusTexts[Response::HTTP_NOT_FOUND];
        }

        parent::__construct(null, Response::HTTP_NOT_FOUND, $message, true);
    }
}
