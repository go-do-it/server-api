<?php

namespace AppBundle\Api\Result;

use Symfony\Component\HttpFoundation\Response;

class CreatedResult extends AbstractResult
{
    /**
     * @inheritDoc
     */
    public function __construct($data = null, $message = null)
    {
        parent::__construct($data, Response::HTTP_CREATED, $message);
    }
}
