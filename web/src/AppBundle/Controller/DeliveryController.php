<?php

namespace AppBundle\Controller;

use AppBundle\Api\Result\BadRequestResult;
use AppBundle\Api\Result\CreatedResult;
use AppBundle\Api\Result\NotFoundResult;
use AppBundle\Api\Result\OkResult;
use AppBundle\Entity\Delivery;
use AppBundle\Form\Type\DeliveryType;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;


class DeliveryController extends FOSRestController
{
    /**
     * Get user delivery
     *
     * @ApiDoc(
     *      resource="/api/users/{identifier}/delivery",
     *      description="Get user delivery",
     *     requirements={
     *          {
     *              "name"="identifier",
     *              "dataType"="string",
     *              "requirement"="[\w]+",
     *              "description"="user identifier"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Delivery not found",
     *          Response::HTTP_OK="Delivery found"
     *      }
     * )
     *
     * @Get(
     *     path="users/{identifier}/delivery",
     *     name="user_delivery_get",
     *     requirements={"identifier": "\w+"}
     * )
     *
     * @View(serializerGroups={"result"})
     *
     * @param string $identifier
     * @return OkResult|\FOS\RestBundle\View\View
     */
    public function getUserDeliveryAction(string $identifier)
    {
        $delivery = $this->get('doctrine.orm.entity_manager')->getRepository(Delivery::class)->findOneBy([
            'identifier' => $identifier,
        ]);

        if (!$delivery) {
            return $this->view(new NotFoundResult(), Response::HTTP_NOT_FOUND);
        }

        return new OkResult($delivery);
    }

    /**
     * Post user delivery
     *
     * @ApiDoc(
     *     resource="/api/users/{identifier}/delivery",
     *     description="Post user delivery",
     *     requirements={
     *          {
     *              "name"="identifier",
     *              "dataType"="string",
     *              "requirement"="[\w]+",
     *              "description"="user identifier"
     *          }
     *      },
     *      parameters={
     *            { "name"="data", "dataType"="string", "format" = ".+", "required"=true},
     *      },
     *      statusCodes={
     *          Response::HTTP_BAD_REQUEST ="Bad request or invalid data",
     *          Response::HTTP_CREATED="Delivery created"
     *      }
     * )
     *
     * @Post(
     *     path="users/{identifier}/delivery",
     *     name="user_delivery_post",
     *     requirements={"identifier": "\w+"}
     * )
     *
     * @View(statusCode=Response::HTTP_CREATED)
     *
     * @param Request $request
     * @param string  $identifier
     * @return \FOS\RestBundle\View\View
     */
    public function postUserDeliveryAction(Request $request, string $identifier): \FOS\RestBundle\View\View
    {
        $form = $this->createForm(DeliveryType::class);

        $data = $request->request->all();
        $data['identifier'] = $identifier;

        $form->submit($data);
        if ($form->isValid()) {
            $delivery = $form->getData();
            $em = $this->get('doctrine.orm.entity_manager');

            $em->persist($delivery);
            $em->flush($delivery);

            return $this->view(new CreatedResult(['id'=>$delivery->getId()]))->setHeader('ETag', $delivery->getId());
        }

        return $this->view(new BadRequestResult($form))->setStatusCode(Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete user delivery
     *
     * @ApiDoc(
     *     resource="/api/users/{identifier}/delivery",
     *     description="Delete product",
     *     requirements={
     *          {
     *              "name"="identifier",
     *              "dataType"="string",
     *              "requirement"="[\w]+",
     *              "description"="user identifier"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Delivery not found",
     *          Response::HTTP_NO_CONTENT="Delivery was deleted"
     *      }
     * )
     *
     * @Delete(
     *     path="users/{identifier}/delivery",
     *     name="user_delivery_delete",
     *     requirements={"identifier": "\w+"}
     * )
     *
     * @View(statusCode=Response::HTTP_NO_CONTENT)
     *
     * @param string $identifier
     * @return \FOS\RestBundle\View\View|null
     */
    public function deleteUserDeliveryAction(string $identifier)
    {
        $delivery = $this->get('doctrine.orm.entity_manager')->getRepository(Delivery::class)->findOneBy([
            'identifier' => $identifier,
        ]);

        if (!$delivery) {
            return $this->view(new NotFoundResult(), Response::HTTP_NOT_FOUND);
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($delivery);
        $em->flush($delivery);

        return null;
    }
}
