<?php

namespace AppBundle\Controller;

use AppBundle\Api\Result\AbstractResult;
use AppBundle\Api\Result\NotFoundResult;
use AppBundle\Api\Result\OkResult;
use AppBundle\Entity\Action;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;


class ActionController extends FOSRestController
{
    /**
     * Get actions
     *
     * @ApiDoc(
     *      resource="/api/actions",
     *      description="Get all actions",
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Actions not found",
     *          Response::HTTP_OK="Actions found"
     *      }
     * )
     *
     * @Get(
     *     path="actions",
     *     name="actions_get"
     * )
     *
     * @View(serializerGroups={"result"})
     *
     * @return AbstractResult
     */
    public function getActionsAction(): AbstractResult
    {
        $actions = $this->get('doctrine.orm.entity_manager')->getRepository(Action::class)->findAll();

        if (!$actions) {
            return new NotFoundResult();
        }

        return new OkResult($actions);
    }

    /**
     * Get an action
     *
     * @ApiDoc(
     *      resource="/api/actions/{id}",
     *      description="Get an action",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="Action id"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Action not found",
     *          Response::HTTP_OK="Action found"
     *      }
     * )
     *
     * @Get(
     *     path="actions/{id}",
     *     name="action_get",
     *     requirements={"id"="[0-9]+"}
     * )
     *
     * @View(serializerGroups={"result"})
     *
     * @param Action $action
     * @return AbstractResult
     */
    public function getAction(Action $action): AbstractResult
    {
        return new OkResult($action);
    }
}
