<?php

namespace AppBundle\Controller;

use AppBundle\Api\Result\BadRequestResult;
use AppBundle\Api\Result\CreatedResult;
use AppBundle\Api\Result\NotFoundResult;
use AppBundle\Api\Result\OkResult;
use AppBundle\Entity\Product;
use AppBundle\Form\Type\ProductType;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;


class ProductController extends FOSRestController
{
    /**
     * Get user products
     *
     * @ApiDoc(
     *      resource="/api/users/{identifier}/products",
     *      description="Get all user products",
     *     requirements={
     *          {
     *              "name"="identifier",
     *              "dataType"="string",
     *              "requirement"="[\w]+",
     *              "description"="user identifier"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Products not found",
     *          Response::HTTP_OK="Products found"
     *      }
     * )
     *
     * @Get(
     *     path="users/{identifier}/products",
     *     name="user_products_get",
     *     requirements={"identifier": "\w+"}
     * )
     *
     * @View(serializerGroups={"result"})
     *
     * @param string $identifier
     * @return OkResult|\FOS\RestBundle\View\View
     */
    public function getUserProductsAction(string $identifier)
    {
        $products = $this->get('doctrine.orm.entity_manager')->getRepository(Product::class)->findBy([
            'identifier' => $identifier,
        ]);

        if (!$products) {
            return $this->view(new NotFoundResult(), Response::HTTP_NOT_FOUND);
        }

        return new OkResult($products);
    }

    /**
     * Post user product
     *
     * @ApiDoc(
     *     resource="/api/users/{identifier}/product",
     *     description="Post user product",
     *     requirements={
     *          {
     *              "name"="identifier",
     *              "dataType"="string",
     *              "requirement"="[\.]+",
     *              "description"="user identifier"
     *          }
     *      },
     *      parameters={
     *            { "name"="url", "dataType"="string", "format" = ".+", "required"=true},
     *            { "name"="price", "dataType"="float", "format" = "d+", "required"=true},
     *            { "name"="price_expected", "dataType"="float", "format"="d+", "required"=true}
     *      },
     *      statusCodes={
     *          Response::HTTP_BAD_REQUEST ="Bad request or invalid data",
     *          Response::HTTP_CREATED="Product created"
     *      }
     * )
     *
     * @Post(
     *     path="users/{identifier}/product",
     *     name="user_products_post",
     *     requirements={"identifier": "\w+"}
     * )
     *
     * @View(statusCode=Response::HTTP_CREATED)
     *
     * @param Request $request
     * @param string  $identifier
     * @return \FOS\RestBundle\View\View
     */
    public function postUserProductAction(Request $request, string $identifier)
    {
        $form = $this->createForm(ProductType::class);

        $data = $request->request->all();
        $data['identifier'] = $identifier;

        $form->submit($data);
        if ($form->isValid()) {
            $product = $form->getData();
            $em = $this->get('doctrine.orm.entity_manager');

            $em->persist($product);
            $em->flush($product);

            return $this->view(new CreatedResult(['id'=>$product->getId()]))->setHeader('ETag', $product->getId());
        }

        return $this->view(new BadRequestResult($form))->setStatusCode(Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete user product
     *
     * @ApiDoc(
     *      resource="/api/users/{identifier}/products/{url}",
     *      description="Delete product",
     *     requirements={
     *          {
     *              "name"="identifier",
     *              "dataType"="string",
     *              "requirement"="[\w]+",
     *              "description"="user identifier"
     *          },
     *          {
     *              "name"="url",
     *              "dataType"="string",
     *              "requirement"="[\.]+",
     *              "description"="url"
     *          }
     *      },
     *      statusCodes={
     *          Response::HTTP_NOT_FOUND="Product not found",
     *          Response::HTTP_NO_CONTENT="Product was deleted"
     *      }
     * )
     *
     * @Delete(
     *     path="users/{identifier}/products/{url}",
     *     name="user_products_delete",
     *     requirements={"identifier": "\w+", "url": "\.+"}
     * )
     *
     * @View(statusCode=Response::HTTP_NO_CONTENT)
     *
     * @param string $identifier
     * @param string $url
     * @return \FOS\RestBundle\View\View|null
     */
    public function deleteUserProductAction(string $identifier, string $url)
    {
        $product = $this->get('doctrine.orm.entity_manager')->getRepository(Product::class)->findOneBy([
            'identifier' => $identifier,
            'url'        => $url,
        ]);

        if (!$product) {
            return $this->view(new NotFoundResult(), Response::HTTP_NOT_FOUND);
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($product);
        $em->flush($product);

        return null;
    }
}
